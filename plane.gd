extends Node2D


export var direction = -1
const MOVE_SPEED = 3
var is_driving = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if direction == -1:
		$CanvasLayer/plane_warning.position.x = 970.258
		$PlaneSprite.scale.x = 1
	else:
		$CanvasLayer/plane_warning.position.x = 50
		$PlaneSprite.scale.x = -1
	
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if is_driving:
		global_position.x += direction * MOVE_SPEED
	if global_position.x >= 1300 || global_position.x <= -100:
		queue_free()
	


func _on_CollisionDetection_body_entered(body):
	if body.is_in_group("bounceguy"):
		body.hit_something()

func start_driving():
	is_driving = true
