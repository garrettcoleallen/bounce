extends Node2D

export var value = 5

signal collected

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group("bounceguy"):
		emit_signal("collected", value)
		$Area2D/CollisionShape2D.disabled = true
		$AnimationPlayer.play("collected")
