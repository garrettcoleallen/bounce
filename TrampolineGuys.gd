extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const GRAVITY = 15
const MOVE_ACCEL = 120
const MAX_SPEED = 250
const MOVE_DECEL = 80

var velocity = Vector2()
export var is_under_control = false


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Physics process stuff yayeet
func _physics_process(delta):
	player_movement(delta)
	
var dead = false
func trampoline_death(left_right):
	if dead:
		return
	dead = true
	$AnimationPlayer.play("death")
	$trampoline.visible = false
	$TrampolineBody.free()
	$AnimationPlayer.play("death")
		

const MAX_RUN_SPEED = 250
const DECEL = 80
const ACCEL = 120
const JUMP_ACCEL = 450
const MAX_FALL_SPEED = 350
var movement_vector = Vector2()
func player_movement(delta):
	
	var left_right = 0
	var run_accel_modifier = 1
	
	if is_under_control && !dead:
		if Input.is_action_pressed("ui_right"):
			left_right = 1
		elif Input.is_action_pressed("ui_left"):
			left_right = -1
		
	if !is_on_floor():
		run_accel_modifier = 0.4
			
	# no left or right pressed so decel
	if abs(left_right) == 0:
		if abs(movement_vector.x) < DECEL:
			movement_vector.x = 0
		else: 
			movement_vector.x -= (movement_vector.x / abs(movement_vector.x)) * DECEL
		
	if abs(movement_vector.x) > MAX_RUN_SPEED:
		movement_vector.x = (movement_vector.x / abs(movement_vector.x)) * MAX_RUN_SPEED
	else:
		movement_vector.x += left_right * ACCEL * run_accel_modifier
		
	if !dead:
		if abs(movement_vector.x) > 0:
			if $AnimationPlayer.current_animation != "move":
				$AnimationPlayer.play("move")
		else:
			$AnimationPlayer.play("idle")
		
	if (Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_accept")) and ((is_on_floor()) and $JumpTimer.is_stopped()) && !dead:
		movement_vector.y = -JUMP_ACCEL
		$JumpTimer.start()
	elif !is_on_floor():
		if !dead:
			$AnimationPlayer.play("jump")
		movement_vector.y += GRAVITY
		# only checking on fall so jump can be fast
		if movement_vector.y > MAX_FALL_SPEED:
			movement_vector.y = MAX_FALL_SPEED
	else:
		movement_vector.y = 1 #fuck me
	
	
	move_and_slide(movement_vector, Vector2(0,-1))
