extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const MAX_Y = -2600
const GRAVITY = Vector2(0, 7.3)

const BOUNCE_MULTIPLIER = Vector2(0, -0.1)

const MAX_ROTATE_SPEED = 15
const BOUNCE_MINIMUM = -8

const MAX_X_SPEED = 150
const X_SPEED_ACCEL = 80
const BOUNCE_CONVERSION = 0.85

var bounces = 0

var velocity = Vector2()
var rng = RandomNumberGenerator.new()
var rotate_speed = 0

var hit_something = false

export var is_under_control = false



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
		
	if is_under_control && !hit_something:
		if Input.is_action_pressed("ui_right"):
			velocity.x += X_SPEED_ACCEL
		elif Input.is_action_pressed("ui_left"):
			velocity.x -= X_SPEED_ACCEL
		
		
	if abs(velocity.x) > MAX_X_SPEED:
		velocity.x = sign(velocity.x) * MAX_X_SPEED
		
	velocity += GRAVITY
	move_and_slide(velocity)
	if global_position.y > 650:
		global_position.y = -200
	
	if hit_something:
		$bounce_guy.frame = 1
		
	
	
	var slide_count = get_slide_count()
	if slide_count && !hit_something:
		for i in range(slide_count):
			var collision = get_slide_collision(i)
			var collider = collision.collider			
			if collision.normal.y == -1 && $BounceTimer.is_stopped() && collision.collider_shape.is_in_group("trampoline"):
				$BounceTimer.start()
				bounces += 1
				velocity.y = -abs(velocity.y)
				velocity.y += collider.get_parent().movement_vector.y
				velocity.y *= BOUNCE_CONVERSION
				velocity.x += collider.get_parent().movement_vector.x
				
				if velocity.y > BOUNCE_MINIMUM:
					velocity.y = BOUNCE_MINIMUM
				rotate_speed = rng.randf_range(-MAX_ROTATE_SPEED, MAX_ROTATE_SPEED)
				move_and_slide(velocity)
				break
			
	rotate(delta*rotate_speed)
	var max_x = get_viewport_rect().end.x - 16
	var min_x = get_viewport_rect().position.x + 16
	if global_position.x >= max_x:
		global_position.x = max_x
	elif global_position.x <= min_x:
		global_position.x = min_x
		
	if global_position.y < MAX_Y:
		global_position.y = MAX_Y
	
	
func calculate_zone():
	var guy_height = global_position.y
	if guy_height > 0:
		return 1
	elif guy_height > -600:
		return 2
	elif guy_height > -1200:
		return 3
	elif guy_height > -1800:
		return 4
	else:
		return 5
		
func hit_something():
	hit_something = true
		
func game_over():
	$bounce_guy.frame = 1
	set_physics_process(false)
	set_process(false)

	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
