extends Node2D


export var show_submit_score = false
export var submit_score_value = 0
export var submit_score_name = ""
export var submit_score_meta = {}
export var game_name = "bounce-prod"
export var show_new_game_button = true

# Called when the node enters the scene tree for the first time.
func _ready():
	_refresh_leaderboard()
	$CanvasLayer/ScoreSubmitModal.visible = show_submit_score
	$CanvasLayer/ScoreSubmitModal/Panel/ScoreLabel/ScoreValue.text = str(submit_score_value)
	$CanvasLayer/ScoreSubmitModal/Panel/NameLabel/NameValue.text = str(submit_score_name)
	$CanvasLayer/Leaderboard/NewGameButton.visible = show_new_game_button

func _process(delta):
	pass

func _refresh_leaderboard(asc = false):
	$GetLeaderboardRequest.request("https://lb.userdefined.io/games/" + game_name + "?asc=" + str(asc) + "&limit=10", PoolStringArray(), false, HTTPClient.METHOD_GET)

func _on_SubmitScoreButton_pressed():
	$CanvasLayer/ScoreSubmitModal/Panel/SubmitScoreButton.visible = false
	var request = JSON.print({ name = submit_score_name,
		score = submit_score_value,
		game = game_name,
		metaData = JSON.print(submit_score_meta) })
	var headers = PoolStringArray()
	headers.append("Content-Type: application/json")
	$SubmitLeaderboardScore.request("https://lb.userdefined.io/games/submit", headers, false, HTTPClient.METHOD_POST, request)

func _on_GetLeaderboardRequest_request_completed(result, response_code, headers, body):
	_clear_leaderboard()
	var json = JSON.parse(body.get_string_from_utf8())
	var top10 = 1
	for item in json.result:
		if top10 > 10:
			break		
		
		$CanvasLayer/Leaderboard/Panel/Top10Grid.add_child(_new_leaderboard_cell("#" + str(top10)))
		$CanvasLayer/Leaderboard/Panel/Top10Grid.add_child(_new_leaderboard_cell(item.name))
		$CanvasLayer/Leaderboard/Panel/Top10Grid.add_child(_new_leaderboard_cell(str(item.score)))
		top10 += 1

func _clear_leaderboard():
	for child in $CanvasLayer/Leaderboard/Panel/Top10Grid.get_children():
		$CanvasLayer/Leaderboard/Panel/Top10Grid.remove_child(child)
	
	$CanvasLayer/Leaderboard/Panel/Top10Grid.add_child(_new_leaderboard_cell(""))
	$CanvasLayer/Leaderboard/Panel/Top10Grid.add_child(_new_leaderboard_cell("Name"))
	
	$CanvasLayer/Leaderboard/Panel/Top10Grid.add_child(_new_leaderboard_cell("Score"))
	
func _new_leaderboard_cell(text):
	var lbl = Label.new()
	lbl.text = text
	lbl.size_flags_horizontal = lbl.SIZE_EXPAND_FILL
	lbl.size_flags_vertical = lbl.SIZE_EXPAND_FILL
	lbl.align = lbl.ALIGN_CENTER
	return lbl

func _on_SubmitLeaderboardScore_request_completed(result, response_code, headers, body):
	_refresh_leaderboard()
	$CanvasLayer/ScoreSubmitModal.visible = false

func _on_NameValue_text_changed():
	submit_score_name = $CanvasLayer/ScoreSubmitModal/Panel/NameLabel/NameValue.text
	$CanvasLayer/ScoreSubmitModal/Panel/SubmitScoreButton.disabled = len(submit_score_name) < 3


func _on_NewGameButton_pressed():
	get_tree().change_scene("res://Game.tscn")


func _on_CloseScoreSubmitButton_pressed():
	$CanvasLayer/ScoreSubmitModal.visible = false
