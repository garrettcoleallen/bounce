extends Node2D

var score = 0

var game_over = false

var leaderboard_scene = preload("res://leaderboard/Leaderboard.tscn")

const MAX_COINS = 8


var car_scene = preload("res://car.tscn")
var plane_scene = preload("res://plane.tscn")
var basketball_scene = preload("res://BasketBall.tscn")
var obstacle_scenes = {}

var coin_scene = preload("res://Coin.tscn")

var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	$CanvasLayer/Seed.text = str(rng.seed)
	_prep_obstacle_options()
	_spawn_coins()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$GuyCamera.global_position.y = $BounceGuy.global_position.y
	if $GuyCamera.global_position.y > 400:
		$GuyCamera.global_position.y = 400
	
	if Input.is_action_just_pressed("camera_swap"):
		if $GuyCamera.current:
			$BounceGuy.is_under_control = false
			$TrampolineGuys.is_under_control = true
			$TrampolineCamera.current = true
		else:
			$BounceGuy.is_under_control = true
			$TrampolineGuys.is_under_control = false
			$GuyCamera.current = true
	
func _physics_process(delta):
	var score_zone = $BounceGuy.calculate_zone()
	
	$CanvasLayer/Score/ScoreValue.text = str(score)
	
	# TODO - fix height to say a zone instead 
	var guy_height = $BounceGuy.global_position.y
	if score_zone == 1:
		$CanvasLayer/Height.text = "Height " + str(round(abs($BounceGuy.global_position.y - 517) / 12)) + " ft"
	elif score_zone == 2:
		$CanvasLayer/Height.text = "in the sky"
	elif score_zone == 3:
		$CanvasLayer/Height.text = "in the stratosphere"
	elif score_zone == 4:
		$CanvasLayer/Height.text = "almost to space"
	else:
		$CanvasLayer/Height.text = "in space"
	
	$CanvasLayer/BounceGuyIndicator.visible = $BounceGuy.global_position.y < 0 && $TrampolineCamera.current
	$CanvasLayer/BounceGuyIndicator/bounce_indicator.global_position.x = $BounceGuy.global_position.x
	$CanvasLayer/BounceGuyIndicator/bounce_indicator.scale.x = abs(1000 / $BounceGuy.global_position.y)
	if $CanvasLayer/BounceGuyIndicator/bounce_indicator.scale.x < 0.5:
		$CanvasLayer/BounceGuyIndicator/bounce_indicator.scale.x = 0.5
	if $CanvasLayer/BounceGuyIndicator/bounce_indicator.scale.x > 4:
		$CanvasLayer/BounceGuyIndicator/bounce_indicator.scale.x = 4
	
	$CanvasLayer/TrampolineIndicators.visible = $GuyCamera.global_position.y < 160 && $GuyCamera.current
	$CanvasLayer/TrampolineIndicators/left_indicator.global_position.x = $TrampolineGuys/holder_guy_left.global_position.x
	$CanvasLayer/TrampolineIndicators/right_indicator.global_position.x = $TrampolineGuys/holder_guy_right.global_position.x


func _prep_obstacle_options():
	var car_right = car_scene.instance()
	car_right.direction = -1
	car_right.global_position = $RightCarPos.global_position
	obstacle_scenes["car_right"] = car_right
	
	var car_left = car_right.duplicate()
	car_left.direction = 1
	car_left.global_position = $LeftCarPos.global_position
	obstacle_scenes["car_left"] = car_left
	
	var plane = plane_scene.instance()
	for i in range(1,5):
		var plane_right = plane.duplicate()
		plane_right.direction = -1
		plane_right.global_position = get_node("RightPlane" + str(i)).global_position
		obstacle_scenes["plane_right_" + str(i)] = plane_right
		var plane_left = plane_right.duplicate()
		plane_left.direction = 1
		plane_left.global_position = get_node("LeftPlane" + str(i)).global_position
		obstacle_scenes["plane_left_" + str(i)] = plane_left
	
	var bb = basketball_scene.instance()
	bb.global_position = $BasketBallPos.global_position
	bb.applied_torque = -3
	obstacle_scenes["basketball"] = bb
	
func score_zone_multiplier(zone):
	match zone:
		1:
			return 1
		2:
			return 3
		3:
			return 5
		4:
			return 8
		5:
			return 15
		_:
			return 1
			
func _on_GameOverArea_body_entered(body):
	$BounceGuy.game_over()
	game_over = true
	var lb = leaderboard_scene.instance()
	lb.game_name = "bounce-prod"
	lb.show_submit_score = true
	lb.submit_score_value = score
	lb.submit_score_meta = {} #todo fill this out
	
	add_child(lb)
	print("Game over")

func _spawn_coins():
	var coins_to_spawn = MAX_COINS - $Coins.get_child_count()
	for i in range(0, coins_to_spawn):
		var x = rng.randf_range(32, get_viewport_rect().end.x-32)
		var y = rng.randf_range(-64, -2700)
		var score = _get_coin_value(x,y)
		var coin = coin_scene.instance()
		coin.value = score
		coin.global_position = Vector2(x,y)
		coin.connect("collected", self, "_on_Coin_collected")
		$Coins.add_child(coin)

func _get_coin_value(x,y):
	if y < -1800:
		return 25
	if y < -1326:
		return 10
	if y < -76:
		return 5
	return 1
		

var obstacle_time = 8
const MIN_OB_TIME = 4

func _on_ObstacleTimer_timeout():
	var obstacle_idx = rng.randi_range(0, len(obstacle_scenes.keys())-1)
	var obstacle = obstacle_scenes[obstacle_scenes.keys()[obstacle_idx]].duplicate()
	$Obstacles.add_child(obstacle)
	$ObstacleTimer.start(obstacle_time)
	if obstacle_time > MIN_OB_TIME:
		obstacle_time -= 0.5
	


func _on_Coin_collected(value):
	score += value


func _on_CoinTimer_timeout():
	_spawn_coins()
